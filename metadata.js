function isHidden() {
    return document.hidden === true;
}

function updateMetadata (first) {
    if(status === 'pause' && isHidden()) { // let's save bandwidth
        return true;
    }

    fetch(config.metadata.url,
        {
            cache: first ? 'reload' : 'no-cache',   // using no-cache, client will make a conditional request (If-Modified-Since), if possible
        })
        .then(function(response) {
        if(response.status === 200) {
            return response.json();
        }
        if(first) {
            console.error("Could not find metadata");
        }
    }).then(function(jsonResponse) {
        if(jsonResponse === undefined) { return; }
        let text = jsonResponse.comment;
        if(text.trim() === "//") {
            text = "Selezioni musicali";
        }
        if(text === "") {
            // yes, it's a space. the point is to avoid the row from changing the height
            text = " ";
        }
        document.getElementById('metadata').innerText = text;
    });

    return true;
}

if(config.metadata.url !== undefined) {
    window.addEventListener('load', () => updateMetadata(true))
    setInterval(() => updateMetadata(false), 30 * 1000)

    // since metadata is not updated when the browser is not visible, let's reload
    document.addEventListener("visibilitychange", () => {
        updateMetadata(true);
    });
}
