const ical = require('ical.js')
const fs = require('fs')

fs.readFile('./palinsesto.ics', 'utf8', (err, data) => {
  const ret = ical.parse(data)
  const events = ret[2].filter(e => e[0] === 'vevent')
  console.error(events)
  const j = events.map(e => {
    const details = e[1]
    console.error(details)
    const r = {}
    details.forEach(d => {
      switch (d[0]) {
        case 'summary':
          r.name = d[3]
          break;
        case 'description':
          r.description = d[3]
          break
        case 'rrule':
          r.day = d[3].byday
          break;
        case 'dtstart':
          const date = new Date(d[3])
          r.from_hm = date.getHours() + ':' + date.getMinutes()
          break;
        case 'dtend':
          break;
      }
    })
    return r;
  })
  console.error(j)

})
