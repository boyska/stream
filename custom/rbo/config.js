var config = {
    stream: {
        ogg: {
            high: 'https://s.streampunk.cc/blackout.ogg',
            low:  'https://s.streampunk.cc/blackout-low.ogg',
        },
        default: {
            high: 'https://s.streampunk.cc/blackout.mp3',
            low:  'https://s.streampunk.cc/blackout-low.mp3',
        },
    },
    artwork: {
        src: 'https://cdn.radioblackout.org/wp-content/uploads/2016/03/cropped-testa_rbo_pugno-3-300x300.png'
    }
}
