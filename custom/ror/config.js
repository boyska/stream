var config = {
    stream: {
        ogg: {
            high: 'https://s.streampunk.cc/ondarossa.ogg',
            low:  'https://s.streampunk.cc/ondarossa-low.ogg',
        },
        default: {
            high: 'https://s.streampunk.cc/ondarossa.mp3',
            low:  'https://s.streampunk.cc/ondarossa-low.mp3',
        },
    },
    artwork: {
        src: 'http://www.ondarossa.info/favicon.png',
    },
    metadata: {
        url: "https://player.ondarossa.info/metadata.json",
    }
}

