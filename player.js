/**
* made with bestemmie - blacktech 2020
*/

let status = 'pause'
const el_button = document.getElementById('button')
const el_volume = document.getElementById('volume')

const player = document.createElement("audio")


function getStreamingUrl () {
  /**
  * In modern browser / OS, users could choose to save data on special condition.
  * A network is classified as metered when the user is sensitive to heavy data usage on that connection due to monetary costs, 
  * data limitations or battery/performance issues.
  */
  const connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection
  const quality = (connection === undefined || connection.saveData !== true) ? 'high' : 'low'

  if (config.stream.ogg !== undefined && player.canPlayType('audio/ogg;')) {
    // using Math.random as it fixes a crazy bug that happens only on:
    // - firefox
    // - for stream ogg
    // - in https
    // - already visited (this is the only one we can)
    return config.stream.ogg[quality] + '?' + Math.random();
  } else {
    return config.stream.mp3[quality] + '?' + Math.random();
  }
}

player.addEventListener('error', retryOnError)
player.addEventListener('onloadedmetadata', () => {
  console.error('metadata', arguments)
})

// support mediaSession
if (navigator.mediaSession) {
  navigator.mediaSession.setActionHandler('play', togglePlay)
  navigator.mediaSession.setActionHandler('pause', togglePlay)
  navigator.mediaSession.metadata = new MediaMetadata({
    artwork: [ { src: config.artwork.src }]
  })

}


// enough of the audio has loaded to allow playback to begin
player.addEventListener('canplaythrough', function () {
  el_button.classList.remove('loading')
})

el_button.addEventListener('click', togglePlay )
function togglePlay () {
  if (status === 'play') {
    if(player.stop !== undefined) {
      player.stop()
    } else { // HTMLAudio
      player.pause()
      player.src = ""
    }
    status = 'pause';
  } else {
    player.src = getStreamingUrl()
    player.play()
    status = 'play'
  }
  el_button.classList.toggle('pause')  
}


/** retry on case of error */
player.addEventListener('error', retryOnError)
function retryOnError (e) {
  if(status === 'play') {
    togglePlay()
    setTimeout(togglePlay, 1500)
  }
}


/** VOLUME CHANGE */
function changeVolume (v) {
  player.volume = volume.value/100
}

if(el_volume !== null) {
    el_volume.addEventListener('mousemove', changeVolume)
    el_volume.addEventListener('change', changeVolume)
}
